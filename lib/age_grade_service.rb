require 'open3'
require 'cgi'

class AgeGradeService
  WMA_YEAR_DEFAULT    = 2006
  HOST_DEFAULT        = "http://agcs.herokuapp.com"
  API_VERSION_DEFAULT = 'v1'
  include Open3

  attr_reader :event_type, :event, :event_distance, :athlete, :age_grade_results,
    :athlete_result, :event_date

  def initialize(params)
    @athlete     = params.fetch(:athlete)
    @event       = params.fetch(:event)
    @athlete_result = params.fetch(:athlete_result)

    @event_distance  = event.distance
    @event_type      = event.event_type
    @event_date      = event.date_on

    @host        = params.fetch(:host, HOST_DEFAULT)
    @wma_year    = params.fetch(:wma_year, WMA_YEAR_DEFAULT)
    @api_version = params.fetch(:api_version, API_VERSION_DEFAULT)


    @age = athlete.age_on_date(event_date)
    @url = "#{host}/#{wma_year}/#{api_version}/#{event_type}/#{event_distance}/#{athlete_result}/#{@age}/#{athlete.gender}"
  end

  def get_age_grade
    puts "\t#{athlete.fullname} (#{@age}): curl -s #{url}"
    popen3('curl', '-s', url ) do |stdin, stdout, stderr|
      stdin.close_write
      stdout.read.split("\n").each do |line|
        @age_grade_results = JSON.parse(line)
      end

      stderr.read.split("\n").each do |line|
       puts "[parent] stderr: #{line}"
      end
    end
    return true
  end

  def age_performance_as_percentage
    age_grade_results.fetch('age_performance_as_percentage')
  end

  def age_grade_result
    age_grade_results.fetch('age_grade_result')
  end

  def open_standard
    age_grade_results.fetch('open_standard')
  end

  def age_standard
    age_grade_results.fetch('age_standard')
  end

  def age_factor
    age_grade_results.fetch('age_factor')
  end

  def open_standard_raw
    age_grade_results.fetch('open_standard_raw')
  end

  def age_standard_raw
    age_grade_results.fetch('age_standard_raw')
  end

  private
  attr_reader :host, :wma_year, :api_version, :url
end
