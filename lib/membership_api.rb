require 'open3'
require 'json'
require 'open-uri'
include Open3

class MembershipApi
  PROTOCOL     = 'http'
  HOSTNAME     = 'localhost:3000'
  SERVER_ROUTE = 'fullname'

  attr_reader :fullname, :result

  def initialize(params)
    @fullname = params.fetch(:fullname)
  end

  def request
    encoded_fullname = URI::encode("#{fullname}")
    url = "#{PROTOCOL}://#{HOSTNAME}/#{SERVER_ROUTE}/#{encoded_fullname}"

    popen3('curl', '-s', url) do |stdin, stdout, stderr|
      stdin.close_write
      stdout.read.split("\n").each do |line|
        @result = JSON.parse(line)
      end
    end
  end

  def request_failed?
    @result.has_key?("error") ? true : false
  end

  def message
    @result["error"] ? @result["error"] : ""
  end
end
