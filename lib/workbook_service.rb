require 'rubyXL'
require 'member'
require 'result_workbook'

class WorkbookService
  attr_reader :workbook, :new_spreadsheet, :spreadsheet

  def initialize(params)
    @workbook = params.fetch(:workbook) ##ResultWorkbook.new(params)
  end

  def write_age_performance_as_percentage(row_index, age_grade_result)
    age_grade_result = age_grade_result.instance_of?(Float) ? sprintf("%#.2f", age_grade_result.to_f)
                                                            : age_grade_result
    workbook.write_cell(row_index, 2, age_grade_result)
  end

  def write_spreadsheet(new_spreadsheet = nil)
    workbook.write_spreadsheet(new_spreadsheet)
  end


  private
  attr_reader :workbook, :worksheet, :worksheet_data
end
