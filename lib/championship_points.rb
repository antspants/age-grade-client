require 'rubyXL'
require 'member'
require 'result_workbook'

class ChampionshipPoints
  attr_reader :spreadsheet

  def initialize(params)
    @workbook = ResultWorkbook.new(params)
    @championship_points_collection = {}
  end

  def calculate_championship_points
    collect_age_grades
    delete_invalid_age_grades
    assign_championship_points
    write_spreadsheet
  end

  private
  attr_reader :workbook, :worksheet, :worksheet_data, :championship_points_collection

  private
  def assign_championship_points
    pts = 200
    championship_points_collection.keys.sort.reverse.each do |k|

      championship_points_collection[k].each do |member|
        puts "\t#{pts}: #{member.age_grade}: #{member.fullname}"
        write_championship_points(member, pts)
      end
      pts -= 1
    end
  end

  private
  def write_championship_points(member, points)
    unless member.non_scorer?
      workbook.write_cell(member.index, 3, points)
    end
  end

  private
  def write_spreadsheet
    workbook.write_spreadsheet
  end

  private
  def delete_invalid_age_grades
    championship_points_collection.delete_if do |k,_|
      k.to_s !~ /^\d{1,3}(?:\.\d{1,2})?$/
    end
  end

  private
  def collect_age_grades
    workbook.each do |member|
      if member.non_scorer?
        next
      else
        if @championship_points_collection[member.age_grade].nil?
          @championship_points_collection[member.age_grade] = [member]
        else
          @championship_points_collection[member.age_grade] << member
        end
      end
    end
  end

end
