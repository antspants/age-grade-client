require 'rubyXL'


class ResultWorkbook
  attr_reader :workbook, :worksheet

  ## row index 0
  EVENT_NAME   = 0
  EVENT_DATE   = 1

  ## row index 1
  EVENT_TYPE   = 0
  EVENT        = 1

  ## row index 3 onwards
  FULLNAME     = 0
  RESULT       = 1
  AGE_GRADE    = 2
  POINTS       = 3
  TIMED_RESULT = 4

  def initialize(params)
    @spreadsheet     = params.fetch(:spreadsheet)

    @workbook        = RubyXL::Parser.parse(spreadsheet)
    @worksheet       = workbook[0]
  end

  def worksheet_data
    @worksheet_data ||= worksheet.extract_data.map.with_index { |data, index| [index, data] }
  end

  def event_name
    worksheet_data[0][1][EVENT_NAME]
  end

  def event_date
    worksheet_data[0][1][EVENT_DATE]
  end

  def event_type
    worksheet_data[1][1][EVENT_TYPE]
  end

  def event
    worksheet_data[1][1][EVENT]
  end

  def each(&block)
    worksheet_data[3..-1].each do |data_row|
      raise(ArgumentError.new("Spreadsheet data row is empty")) \
        if data_row.empty?

      next if data_row[1] == nil
      data_row[1][FULLNAME] = sanitise_name(data_row[1][FULLNAME])
      yield Member.new(data_row)
    end
  end

  def write_cell(row, cell, data)
    if worksheet[row][cell].nil?
      worksheet.add_cell(row, cell, data)
    else
      worksheet[row][cell].change_contents(data)
    end
  end

  def write_spreadsheet(new_spreadsheet = spreadsheet)
    puts "writing #{new_spreadsheet}"
    workbook.write(new_spreadsheet)
  end

  private
  def sanitise_name(fullname)
    fullname.strip!
    firstname, *surname = fullname.split

    firstname.downcase!.capitalize!

    surname = surname.each.inject(nil) do |surname, value|
      value.downcase!.capitalize! if /[a-zA-Z]+/.match(value)
      surname = surname.nil? ? value : "#{surname} #{value}"
    end
    return "#{firstname} #{surname}"
  end


  private
  attr_reader :spreadsheet
end
