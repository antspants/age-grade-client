require 'result_transformer'

class Member
  attr_reader :fullname, :result, :age_grade, :points
  attr_accessor :index

  def initialize(data)
    @index     = data[0]
    @fullname  = data[1][0]
    @result    = data[1][1]
    @age_grade = data[1][2]
    @points    = data[1][3]
    @chip_time = data[1][4] ## yet to be implemented
  end

  def non_scorer?
    /([nN]\/[ss]|non-?scorer)/i.match(points.to_s)
  end

  def age_grade
    @age_grade
  end

  def chip_time
    @chip_time
  end

  def result
    Utils::ResultTransformer.transform(@result)
  end
end
