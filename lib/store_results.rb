$:.unshift File.dirname(__FILE__)

require 'database_configuration'
require 'rubyXL'

spreadsheet = ARGV[0]

workbook        = RubyXL::Parser.parse(spreadsheet)
worksheet       = workbook[0]

worksheet_data  = worksheet.extract_data.map.with_index { |data, index| [index, data] }

event = Event.find_or_create_by!(name: workbook.event_name, date_on: workbook.event_date) do |event|
               event.distance   = workbook.event
               event.event_type = workbook.event_type
        end

## whack data into database
event.results.create(timed: workbook.result,
                     chipped: workbook.chipped,
                     age_grade: ags.age_performance_as_percentage,
                     championship_points: workbook.points)

