class User
  attr_reader :dob, :fullname, :firstname, :surname, :gender

  def initialize(params)
    @dob = Date.parse(params["dob"])
    @fullname = params["fullname"]
    @firstname = params["firstname"]
    @surname = params["surname"]
    @gender = params["gender"]
  end

  def age_on_date(event_date)
    now = Date.parse(event_date.to_s)
    now.year - dob.year - ((now.month > dob.month || (now.month == dob.month && now.day >= dob.day)) ? 0 : 1)
  end
end


