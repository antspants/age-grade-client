class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string   :name, null: false
      t.datetime :date_on, null: false
      t.string  :distance, null: false
      t.string   :event_type, null: false

      t.timestamp :created_at
      t.timestamp :updated_at
    end
  end
end

