class CreateResults < ActiveRecord::Migration
  def change
    create_table :results do |t|
      t.belongs_to :event, null: false
      t.string   :timed, null: false
      t.string   :chipped
      t.decimal  :age_grade
      t.integer  :championship_points

      t.timestamp :created_at
      t.timestamp :updated_at
    end
  end
end
