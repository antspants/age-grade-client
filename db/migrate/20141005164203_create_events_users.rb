class CreateEventsUsers < ActiveRecord::Migration
  def change
    create_table :event_users, id: false do |t|
      t.belongs_to :user
      t.belongs_to :event
    end
  end
end
