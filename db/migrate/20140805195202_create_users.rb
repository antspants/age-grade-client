class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :firstname, null: false
      t.string :surname, null: false
      t.date   :dob, null: false
      t.string :gender, limit: 1, null: false
      t.string :address
      t.string :telephone
      t.decimal :subs, precision: 4, scale: 2
      t.string  :membership_year, limit: 4
      t.boolean :mailing_list, default: false
      t.string  :claim_status, linit: 1
      t.date    :subs_paid_on

      t.timestamp :created_at
      t.timestamp :updated_at
    end
  end
end
