# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20141005164204) do

  create_table "event_users", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "event_id"
  end

  create_table "events", :force => true do |t|
    t.string   "name",       :null => false
    t.datetime "date_on",    :null => false
    t.string   "distance",   :null => false
    t.string   "event_type", :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "results", :force => true do |t|
    t.integer  "event_id",            :null => false
    t.string   "timed",               :null => false
    t.string   "chipped"
    t.decimal  "age_grade"
    t.integer  "championship_points"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "firstname",                                                                     :null => false
    t.string   "surname",                                                                       :null => false
    t.date     "dob",                                                                           :null => false
    t.string   "gender",          :limit => 1,                                                  :null => false
    t.string   "address"
    t.string   "telephone"
    t.decimal  "subs",                         :precision => 4, :scale => 2
    t.string   "membership_year", :limit => 4
    t.boolean  "mailing_list",                                               :default => false
    t.string   "claim_status"
    t.date     "subs_paid_on"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
