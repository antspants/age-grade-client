$:.unshift File.dirname(__FILE__)
$:.unshift 'lib'
$:.unshift 'utils'

require 'membership_api'
require 'workbook_service'
require 'age_grade_service'
require 'database_configuration'
require 'user'


puts "\ncalculate age grades: start\n"

spreadsheet = ARGV[0]
result_workbook  = ResultWorkbook.new(spreadsheet: spreadsheet)
workbook_service = WorkbookService.new(workbook: result_workbook)

## create Event
event = Event.find_or_create_by_name_and_event_type_and_date_on!(result_workbook.event_name,
                                                             result_workbook.event_type,
                                                             result_workbook.event_date) do |event|
          event.distance   = result_workbook.event
        end

result_workbook.each do |member|
  begin
    membership_api = MembershipApi.new(fullname: member.fullname)
    membership_api.request

    result = nil
    if membership_api.request_failed?
      $stderr.puts "#{membership_api.message}: #{member.fullname}"
      result  = membership_api.message
    else
      user = User.new(membership_api.result)

      ags = AgeGradeService.new(athlete: user,
                                event: event,
                                athlete_result: member.result)
      ags.get_age_grade
      result = ags.age_performance_as_percentage
    end


    workbook_service.write_age_performance_as_percentage(member.index, result)

  rescue ActiveRecord::RecordNotFound => e
    $stderr.puts "#{e.message} #{member.fullname}"
    workbook_service.write_age_performance_as_percentage(member.index, e.message)
  end
end

new_spreadsheet = File.join(File.dirname(spreadsheet), 'results', File.basename(spreadsheet))
workbook_service.write_spreadsheet(new_spreadsheet)
puts "calculate age grades: end\n"
