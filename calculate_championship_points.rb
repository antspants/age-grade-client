$:.unshift File.dirname(__FILE__)
$:.unshift 'lib'
$:.unshift 'utils'

require 'championship_points'
require 'database_configuration'

puts "\ncalculate championship points: start"
workbook = ChampionshipPoints.new(spreadsheet: ARGV[0])
workbook.calculate_championship_points
puts "calculate championship points: end\n\n"

