require 'active_record'
require 'date'

ActiveRecord::Base.establish_connection(
  adapter: 'postgresql',
  host: 'localhost',
  database: 'hastings_runners',
  pool: 5,
  encoding: 'unicode',
  username: 'postgres',
  password: '',
  port: 5432
)

class Event < ActiveRecord::Base
  has_and_belongs_to_many :users
  has_many :results
end
