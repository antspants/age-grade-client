
module Utils
  class ResultTransformer
    def self.transform(result)
      case result
      when DateTime then result.strftime("%H-%M-%S")
      when String then result.gsub(/[^\d]+/, '-')
      else
        raise ArgumentError "unknown time format #{result}"
      end
    end
  end
end
