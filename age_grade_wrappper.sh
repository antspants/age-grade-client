#!/bin/bash

function rc {
  if [[ $1 != 0 ]]
  then
    echo "problem running $2 return code $1"
    exit 0
  fi
}

spreadsheet="$1"

ruby calculate_age_grades.rb $spreadsheet
rc $? "calculate_age_grade"

results_spreadsheet="$(dirname $spreadsheet)/results/$(basename $spreadsheet)"

ruby calculate_championship_points.rb $results_spreadsheet
rc $? "calculate_championship_points"

ruby store_results.rb $results_spreadsheet
rc $? "store_results"

