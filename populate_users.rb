$:.unshift File.dirname(__FILE__)

require 'date'
require 'rubyXL'
require 'database_configuration'

membership_details = ARGV[0]
raise ArgumentError, "no members spreadsheet provided" \
  if membership_details.nil?

users = User.all
puts "Records to be deleted: #{users.size}"
records_deleted = User.delete_all
puts "Records deleted: #{records_deleted}"

workbook = RubyXL::Parser.parse(membership_details)

(1...352).each do |i|
  puts "processing: #{workbook[0][i][0].value}, #{workbook[0][i][1].value}"
  params = {
    surname: workbook[0][i][0].value.split.inject(nil) do |surname, value|
      value.downcase!
      value.capitalize!
      surname = surname.nil? ? value : "#{surname} #{value}"
    end,
    firstname:        workbook[0][i][1].value.downcase.capitalize,
    gender:           workbook[0][i][2].value,
    dob:              workbook[0][i][3].value
  }
  raise ArgumentError("Incorrect Gender (#{params[:gender]})") \
    unless params[:gender] =~ /^[m|M|f|F]/

  params[:gender] = (params[:gender] =~ /^[f|F]/) ? 'F' : 'M'

  if user = User.find_by_firstname_and_surname(params.fetch(:firstname), params.fetch(:surname))
    user.update_attributes!(params)
    puts "\tupdated: #{user.fullname}"
  else
    user = User.create!(params)
    puts "created: #{user.fullname}"
  end
end

